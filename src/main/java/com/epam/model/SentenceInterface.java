package com.epam.model;

import java.util.List;
import java.util.Map;

public interface SentenceInterface {

    List<Word> findAllWordsInSentence(String s);

    Map<Word, Long> countWordsInSentence(Sentence sentence, List<Word> words);

    Sentence deleteSubLineForTwoSymbols(Sentence sentence, String start, String end);
}
