package com.epam.model;

import java.util.List;
import java.util.Map;

public interface TextInterface {

    List<Sentence> devideTextOnSentence(String text);

    Map<Sentence, Map<Word, Long>> countWordsInEverySentenceInText(String text, List<Word> words);

    List<Sentence> deleteSubLineInEverySentenceForTwoSymbols(String text, String start, String end);

}
