package com.epam.model;

import java.util.List;
import java.util.Objects;

public class Text {

    private String text;
    private List<Sentence> sentences;

    public Text(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text1 = (Text) o;
        return Objects.equals(text, text1.text) &&
                Objects.equals(sentences, text1.sentences);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, sentences);
    }

    @Override
    public String toString() {
        return "Text{" +
                "text='" + text + '\'' +
                ", sentences=" + sentences +
                '}';
    }
}
