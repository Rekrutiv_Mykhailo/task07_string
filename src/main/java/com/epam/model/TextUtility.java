package com.epam.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.epam.model.Helper.PATTERN_SENTENCE;

public class TextUtility implements TextInterface {


    public static void main(String[] args) {
        List<Sentence> sentences = new TextUtility().devideTextOnSentence(Helper.text);
        new TextUtility().deleteSubLineInEverySentenceForTwoSymbols(Helper.text2, "b", "e");
    }

    @Override
    public List<Sentence> devideTextOnSentence(String text) {

        SentenceUtility sentenceUtility = new SentenceUtility();
        Sentence sentence;
        List<Sentence> sentences = new ArrayList<>();
        Pattern p = Pattern.compile(PATTERN_SENTENCE);
        Matcher m = p.matcher(text);
        while (m.find()) {
            // sentence = new Sentence(text.substring(m.start(), m.end() + 1));
            sentence = new Sentence(text.substring(m.start(), m.end()));
            sentence.setWords(sentenceUtility.findAllWordsInSentence(sentence.getSentence()));
            sentences.add(sentence);
        }
        return sentences;
    }

    @Override
    public Map<Sentence, Map<Word, Long>> countWordsInEverySentenceInText(String text, List<Word> words) {

        Text text1 = new Text(text);
        text1.setSentences(devideTextOnSentence(text));
        List<Map<Word, Long>> countWords = text1.getSentences().stream().map(sentence -> new SentenceUtility()
                .countWordsInSentence(sentence, words))
                .collect(Collectors.toList());
        Map<Sentence, Map<Word, Long>> countWordsWithSentence = new LinkedHashMap<>();
        for (int i = 0; i < countWords.size(); i++) {
            countWordsWithSentence.put(text1.getSentences().get(i), countWords.get(i));
        }
        return countWordsWithSentence;
    }

    @Override
    public List<Sentence> deleteSubLineInEverySentenceForTwoSymbols(String text, String start, String end) {
        Text text1 = new Text(text);
        text1.setSentences(devideTextOnSentence(text));
        List<Sentence> sentences = text1.getSentences()
                .stream().
                        map(sentence -> new SentenceUtility()
                                .deleteSubLineForTwoSymbols(sentence, start, end))
                .collect(Collectors.toList());
        return sentences;
    }
}
