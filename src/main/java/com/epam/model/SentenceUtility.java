package com.epam.model;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SentenceUtility implements SentenceInterface {

    public static void main(String[] args) {
//        SentenceUtility sentenceUtility = new SentenceUtility();
//        List<Word> words = new ArrayList<>();
//        Word word = new Word("programming");
//        Word word1 = new Word("is");
//        Word wor2 = new Word("axxxx");
//        words.add(word);
//        words.add(word1);
//        words.add(wor2);
        Sentence sentence = new Sentence();
//        sentence.setWords(sentenceUtility.findAllWordsInSentence(Helper.sentence));
//       // System.out.println(new SentenceUtility().countWordsInSentence(sentence, words));
//        new TextUtility().countWordsInEverySentenceInText(Helper.text,words);
        new SentenceUtility().deleteSubLineForTwoSymbols(null, "e", "b");
    }

    @Override
    public List<Word> findAllWordsInSentence(String sentence) {
        List<Word> words;
        words = Arrays.stream(sentence.replaceAll("[!?,:;()]", "").split("\\s+"))
                .map(Word::new)
                .collect(Collectors.toList());
        return words;
    }

    @Override
    public Map<Word, Long> countWordsInSentence(Sentence sentence, List<Word> words) {
        List<Long> longs = words.stream().map(word -> calculateCountWordInSentence(sentence, word)).collect(Collectors.toList());
        Map<Word, Long> wordsCounts = new LinkedHashMap<>();
        for (int i = 0; i < words.size(); i++) {
            wordsCounts.put(words.get(i), longs.get(i));
        }
        return wordsCounts;
    }

    public Long calculateCountWordInSentence(Sentence sentence, Word word) {
        return sentence.getWords().stream().filter(word::equals).count();
    }

    @Override
    public Sentence deleteSubLineForTwoSymbols(Sentence sentence, String start, String end) {

        String sent = sentence.getSentence();
        int startIndex = sent.indexOf(start);
        int endIndex = sent.lastIndexOf(end);
        if (sent.contains(start) && sent.contains(end) && startIndex <= endIndex) {
            String newSentence = sent.substring(0, startIndex) + sent.substring(endIndex + 1);
            sentence.setSentence(newSentence);
            sentence.setWords(findAllWordsInSentence(sent));
        }
        System.out.println(sentence);
        return sentence;
    }
}
