package com.epam.controller;

public interface MainControllerInterface {
    void doFirstTask();
    void doSecondTask();
    void doThirdTask();
    void doFourthTask();
    void doFifthTask();
    void doSixthTask();
    void doSeventhTask();
    void doEighthTask();
    void doNinthTask();
    void doTenthTask();
    void doElevensTask();
    void doTwelfthTask();
    void doThirteenthTask();
    void doFourteenthTask();
    void doFifteenthTask();
    void doSixteenthTask();
}
