package com.epam.controller;

public class MainController implements MainControllerInterface {
    @Override
    public void doFirstTask() {

    }

    @Override
    public void doSecondTask() {

    }

    @Override
    public void doThirdTask() {

    }

    @Override
    public void doFourthTask() {

    }

    @Override
    public void doFifthTask() {

    }

    @Override
    public void doSixthTask() {

    }

    @Override
    public void doSeventhTask() {

    }

    @Override
    public void doEighthTask() {

    }

    @Override
    public void doNinthTask() {

    }

    @Override
    public void doTenthTask() {

    }

    @Override
    public void doElevensTask() {

    }

    @Override
    public void doTwelfthTask() {

    }

    @Override
    public void doThirteenthTask() {

    }

    @Override
    public void doFourteenthTask() {

    }

    @Override
    public void doFifteenthTask() {

    }

    @Override
    public void doSixteenthTask() {

    }
}
