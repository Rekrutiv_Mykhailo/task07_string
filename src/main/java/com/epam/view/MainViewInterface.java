package com.epam.view;

public interface MainViewInterface {
    void outputFirstTask();
    void outputSecondTask();
    void outputThirdTask();
    void outputFourthTask();
    void outputFifthTask();
    void outputSixthTask();
    void outputSeventhTask();
    void outputEighthTask();
    void outputNinthTask();
    void outputTenthTask();
    void outputElevensTask();
    void outputTwelfthTask();
    void outputThirteenthTask();
    void outputFourteenthTask();
    void outputFifteenthTask();
    void outputSixteenthTask();
}
