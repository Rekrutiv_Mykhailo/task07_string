package com.epam.view;

import com.epam.service.CheckingCorrectMenuException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;


public class AbstractView implements UserCommunication {

    private Logger logger = LogManager.getLogger(AbstractView.class);

    @Override
    public void output(String mess) {
        logger.info(mess);
    }

    @Override
    public int input() {
        int number = 0;
        Scanner in = new Scanner(System.in);
        try {
            number = in.nextInt();
        } catch (InputMismatchException e) {
            logger.error("incorrect iput");
            input();
        }
        return number;
    }

    public int inputMenuCorrect()throws CheckingCorrectMenuException{
        int number = input();
        if (number >=0&&number<=16) {
            return number;
        } else {
                throw new CheckingCorrectMenuException("Please enter 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16 or 0");
        }
    }

    public Integer inputMenu(){
        int numb = Integer.MAX_VALUE;
        try {
            numb = inputMenuCorrect();
        }catch (CheckingCorrectMenuException e) {
            logger.error(e);
            inputMenu();
        }
        return (Integer) numb;
    }
}
