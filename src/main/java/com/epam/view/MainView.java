package com.epam.view;

import com.epam.service.CheckingCorrectMenuException;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainView extends AbstractView implements MainViewInterface {
    private Map<String, String> menu;
    private Map<Integer, Runner> methodsMenu;

    public MainView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        loadMenu();
        System.out.println("menu");
        printMenu(menu);
        output("choose");
        methodsMenu.get(inputMenu()).run();
    }

    public static void main(String[] args) {
        MainView mainView = new MainView();
    }

    public void loadMenu() {

        menu.put("1", "  1 - Find the largest number of sentences in the text that have the same words.");
        menu.put("2", "  2 - Print all sentences of the given text in ascending order of words in each of them.");
        menu.put("3", "  3 - Find the word in the first sentence, which is not in any of the other sentences.");
        menu.put("4", "  4 - In all the questionable sentences of the text, find and print without repetition words of a given length.");
        menu.put("5", "  5 - In each sentence of the text, the first word begins to be swapped the vowel with the longest word.");
        menu.put("6", "  6 - Print words in alphabetical order in the first letter. The words starting with a new letter, type from the paragraph indentation.");
        menu.put("7", "  7 - Sort words by increasing the percentage of vowels.");
        menu.put("8", "  8 - Word words starting with vowels are sorted into alphabetical order in the first consonant letter of the word.");
        menu.put("9", "  9 - All words are sorted by the number of words in a given letter. Words with the same number placed in alphabetical order.");
        menu.put("10", "  10 - There is text and a list of words. For each word from a given list, find how many times it occurs in each sentence, and sort words by decreasing the total number of occurrences.");
        menu.put("11", "  11 - In each sentence of the text, remove the substratum of the maximum length that begins and ends with the given characters.");
        menu.put("12", "  12 - From the text, delete all words of a given length beginning with an accented letter.");
        menu.put("13", "  13 - Sort words in the text by decreasing the number of occurrences of a given character, and in the case of equality - by alphabet.");
        menu.put("14", "  14 - In the given text, find a subset of maximum length, which is a palindrome, that is, read to the left to the right and to the right to the left in the same way.");
        menu.put("15", "  15 - Convert each word in the text by deleting all subsequent (previous) occurrences of the first (last) letter of that word.");
        menu.put("16", "  16 - In a certain sentence of the text, replace the word of a given length with a given substring, whose length may not coincide with the length of the word.");
        menu.put("0", "  0 - finish");
        methodsMenu.put(1, this::outputFirstTask);
        methodsMenu.put(2, this::outputSecondTask);
        methodsMenu.put(3, this::outputThirdTask);
        methodsMenu.put(4, this::outputFourthTask);
        methodsMenu.put(5, this::outputFifthTask);
        methodsMenu.put(6, this::outputSixthTask);
        methodsMenu.put(7, this::outputSeventhTask);
        methodsMenu.put(8, this::outputEighthTask);
        methodsMenu.put(9, this::outputNinthTask);
        methodsMenu.put(10, this::outputTenthTask);
        methodsMenu.put(11, this::outputElevensTask);
        methodsMenu.put(12, this::outputTwelfthTask);
        methodsMenu.put(13, this::outputThirteenthTask);
        methodsMenu.put(14, this::outputFourteenthTask);
        methodsMenu.put(15, this::outputFifteenthTask);
        methodsMenu.put(16, this::outputSixteenthTask);

    }

    public void printMenu(Map<String, String> menu) {

        for (String o : menu.values()) {
            output(o);
        }
    }

    @Override
    public void outputFirstTask() {

        System.out.println("fdlksafdslka");
    }

    @Override
    public void outputSecondTask() {

    }

    @Override
    public void outputThirdTask() {

    }

    @Override
    public void outputFourthTask() {

    }

    @Override
    public void outputFifthTask() {

    }

    @Override
    public void outputSixthTask() {

    }

    @Override
    public void outputSeventhTask() {

    }

    @Override
    public void outputEighthTask() {

    }

    @Override
    public void outputNinthTask() {

    }

    @Override
    public void outputTenthTask() {

    }

    @Override
    public void outputElevensTask() {

    }

    @Override
    public void outputTwelfthTask() {

    }

    @Override
    public void outputThirteenthTask() {

    }

    @Override
    public void outputFourteenthTask() {

    }

    @Override
    public void outputFifteenthTask() {

    }

    @Override
    public void outputSixteenthTask() {

    }

    @Override
    public void output(String mess) {
        super.output(mess);
    }

    @Override
    public int input() {
        return super.input();
    }

    @Override
    public int inputMenuCorrect() throws CheckingCorrectMenuException {
        return super.inputMenuCorrect();
    }

    @Override
    public Integer inputMenu() {
        return super.inputMenu();
    }
}
